<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta http-equiv="refresh" content="300">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>InfoTV</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,900&amp;subset=latin-ext" rel="stylesheet">

        <!-- Scripts -->
        <link href="/js/app.js" />

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #343f4b;
                font-family: 'Source Sans Pro', sans-serif;
                font-weight: 300;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #343f4b;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
            .homepage {
              width: 1280px;
              height: 720px;
            }
            .side {
              width: 300px;
              height: 440px;
              float: left;
            }
            .content {
              width: 980px;
              height: 260px;
              float: left;
              background: #ffffff;
            }
            .bottom {
              float:left;
              height: 260px;
              width: 1280px;
            }
            .clock {
              background: #d2452d;
              height: 88px;
              line-height: 88px;
              font-size: 48px;
              color: white;
              text-align: center;
              font-weight: 700;
            }
            .date {
              background: #a63623;
              height: 95px;
              line-height: 45px;
              font-size: 48px;
              color: white;
              text-align: center;
              font-weight: 700;
            }
            .time {
              float: left;
              width: 70px;
            }
            .status {
              background: #343f4b;
              height: 80px;
              line-height: 80px;
              font-size: 32px;
              padding-left: 24px;
              text-align: left;
            }
            .conditions {
              width: calc(50% - 80px);
              height: 416px;
              padding: 25px 30px 20px;
              background: #222c38;
              float: left;
              text-align: justify;
              font-size: 22px;
              line-height: 35px;
              color: #ffffff;
              font-weight: 500;
            }
            .city {
              margin-bottom: 10px;
            }
            .person{
              margin-bottom: 15px;
            }
            .forecast {
              width: calc(50% - 40px);
              height: 416px;
              padding: 25px 30px 20px;
              float: left;
              text-align: justify;
              font-size: 19px;
              line-height: 28px;
            }
            .forecast p {
              margin: 0 0 15px;
              font-weight: 400;
            }
            .prospects {
              height: 215px;
              background: #4baed0;
              float: left;
              width: 710px;
              text-align: justify;
              font-size: 19px;
              line-height: 28px;
              padding: 25px 30px 20px;
            }
            .prospects p{
              margin: 0 0 15px;
              font-weight: 400;
            }
            .table {
              width: calc(100% - 60px);
              height: 200px;
              padding: 28px 30px 17px;
              float: left;
              text-align: left;
              font-size: 20px;
              line-height: 30px;
            }
            .warning {
              width: 462px;
              height: calc(280px - 68px);
              padding: 24px;
              float: left;
              text-align: justify;
              font-size: 19px;
              line-height: 28px;
              background: #f5f7f8;
            }
            .warning p {
              margin: 0 0 15px;
              font-weight: 400;
            }
            .dark {
              background: #222c38;
            }
            .white {
              background: #ffffff;
            }
            .blue {
              background: #4baed0;
            }
            .text-blue {
              color: #4baed0;
            }
            .dark {
              background: #2a88a8;
            }
            .text-dark {
              color: #2a88a8;
            }
            .light {
              background: #dde8ed;
            }
            .text-light {
              color: #dde8ed;
            }
            .red {
              background: #f9745c;
            }
            .text-red {
              color: #f9745c;
            }
            .grey {
              background: #f5f7f8;
            }
            .title {
              color: #343f4b;
              text-align: left;
              margin: 0 0 15px;
              padding-bottom: 15px;
              border-bottom: 2px solid rgba(52, 63, 75, .3);
              font-size: 26px;
              line-height: 10px;
            }
            .conditions .title {
              color: #ffffff;
              border-color: rgba(255,255,255,.3)
            }
            .less {
              color: #9EC0E4;
            }
            .temperature {
              float: right;
              font-weight: 700;
            }
            .sky {
              float: right;
              text-align: right;
              padding-right: 30px;
              font-size: 16px;
            }
            .bold {
              font-weight: 700;
            }
            .semibold {
              font-weight: 600;
            }
        </style>
        <script>
            function startTime() {
                var today = new Date();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                document.getElementById('clock').innerHTML =
                h + ":" + m + ":" + s;
                var t = setTimeout(startTime, 500);
            }
            function checkTime(i) {
                if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
                return i;
            }
            function checkDate(){
                var days = ['Nedelja', 'Ponedeljek','Torek','Sreda','Četrtek','Petek','Sobota'];
                var today = new Date();
                var d = today.getDate();
                var m = today.getMonth();
                var y = today.getFullYear();
                document.getElementById("date").innerHTML = days[today.getDay()] + '</br>' + d + '.' +(m + 1) + '.' + y;
            }
            function colorSchedule(){
                var d = new Date().getDay();
                var h = new Date().getHours();
                var id;
                if (d >= 1 || d <= 5){
                  if (5 <= h  && h <= 9)  id = 1;
                  if (9 <= h  && h <= 13) id = 2;
                  if (14 <= h && h <= 19) id = 3;
                }
                else {
                  if (7 <= h  && h <= 13)  id = 1;
                  if (13 <= h  && h <= 19) id = 2;
                }
                if (id) {
                  document.getElementById('weekP'+id).className = "person semibold text-blue";
                  document.getElementById('weekT'+id).className = "time bold text-dark";
                }
            }
        </script>
    </head>
    <body onload="startTime(), checkDate(), colorSchedule()">
        <div class="homepage">
          <div class="side">
            <div id="clock" class="clock"></div>
            <div id="date" class="date"></div>
            <div class="table gray">
              <h3 class="title">Urnik</h3>
              @foreach ($data['schedule'] as $para => $id)
              <p>
                <div id = "weekP{{ $loop->iteration }}" class="person">
                {{ $para }}
                <span id = "weekT{{ $loop->iteration }}" class="time bold"> {{ $id }} </span>
                </div>
              </p>
              @endforeach
            </div>
          </div>
          <div class="content">
            <div class="conditions">
              <h3 class="title">Temperature</h3>
              @foreach($data["cities"] as $name => $city)
              <div class="city">
                {{$name}}
                <span class="temperature pull-right bold">{{$city["temp"]}} °C</span>
                <span class="sky pull-right align-right padding-right">{{$city["sky"]}}</span>
              </div>
              @endforeach
            </div>
            <div class="forecast">
              <h3 class="title">Vremenska napoved</h3>
              @foreach ($data['forecast']['forecasts'] as $para)
              <p>
                {{ $para }}
              </p>
              @endforeach
              <p>
              </p>
            </div>
          </div>
          <div class="bottom">
            <div class="prospects">
              <h3 class="title">Obeti</h3>
              @foreach ($data['forecast']['prospects'] as $para)
              <p>
                {{ $para }}
              </p>
              @endforeach
            </div>
            <div class="warning">
              <h3 class="title">Opozorilo</h3>
              @foreach ($data['forecast']['warning'] as $para)
              <p>
                {{ $para }}
              </p>
              @endforeach
            </div>
          </div>
        </div>
    </body>
</html>


<?php
?>
