<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Cache;

Route::get('/', function () {
    $dataCache = Cache::remember('infoCache', 5, function(){
        $data = [ "cities" => array(), "forecast" => array(), "schedule" => array() ];
        $weekday = date('l');

        if ($weekday == "Saturday" || $weekday == "Sunday"){
            $weekday .= (int) (date('d')/7);
        }

        $schedule = [
            "Monday"     => [
                "Matjaž" => "05-09",
                "Taya"   => "09-13",
                "Gregor" => "14-19",
            ],
            "Tuesday"    => [
                "Petra"  => "05-09",
                "Larisa" => "09-13",
                "Jure"   => "14-19",
            ],
            "Wednesday"  => [
                "Mojca"  => "05-09",
                "Taya"   => "09-13",
                "Tina"   => "14-19",
            ],
            "Thursday"   => [
                "Aleš"   => "05-09",
                "Petra"  => "09-13",
                "Jure"   => "14-19",
            ],
            "Friday"     => [
                "Uroš"   => "05-09",
                "Taya"   => "09-13",
                "Petra"  => "14-19",
            ],
            "Saturday0"  => [
                "Uroš"   => "07-13",
                "Gregor" => "13-19",
            ],
            "Sunday0"    => [
                "Sašo"   => "07-13",
                "Petra"  => "13-19",
            ],
            "Saturday1"  => [
                "Mojca"  => "07-13",
                "Gregor" => "13-19",
            ],
            "Sunday1"    => [
                "Franci" => "07-13",
                "Sašo"   => "13-19",
            ],
            "Saturday2"  => [
                "Matjaž" => "07-13",
                "Gregor" => "13-19",
            ],
            "Sunday2"    => [
                "Franci" => "07-13",
                "Uroš"   => "13-19",
            ],
            "Saturday3"  => [
                "Mojca"  => "07-13",
                "Petra"  => "13-19",
            ],
            "Sunday3"    => [
                "Franci" => "07-13",
                "Matjaž" => "13-19",
            ]
        ];

        if (!empty($schedule[$weekday])) 
            $data["schedule"] = $schedule[$weekday];
        
        $sources = [
            [
                "xml" => 'http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_si_latest.xml',
                "cities" => [
                    "Novo mesto" => "NOVO-MES_",
                    "Črnomelj"   => "CRNOMELJ_",
                ]
            ],
            [
                "xml" => 'http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_si_latest.xml',
                "cities" => [
                    "Trebnje"       => "TREBNJE_",
                    "Metlika"       => "METLIKA_",
                    "Škocjan"       => "SKOCJAN_",
                    "Žužemberk"     => "MARIN-VAS_",
                ]
            ],
            [
                "xml" => 'http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observationAms_KRSKO_NEK_latest.xml',
                "cities" => [
                    "Krško" => "KRSKO_NEK_",
                ]
            ],
            [
                "xml" => 'http://meteo.arso.gov.si/uploads/probase/www/observ/surface/text/sl/observation_KOCEVJE_latest.xml',
                "cities" => [
                    "Kočevje" => "KOCEVJE_",
                ]
            ],
        ];

        foreach ($sources as $source) {
            try { $xml = simplexml_load_file($source["xml"]); } catch (Exception $e) {}
            if (!empty($xml)) {
                foreach($source["cities"] as $city => $id) {
                    $cityXml = $xml->xpath("//domain_meteosiId[. = '$id']/..");
                    if (!empty($cityXml)) {
                        $data["cities"][$city]["temp"] = (string) round($cityXml[0]->t);
                        $data["cities"][$city]["sky"]  = (string) $cityXml[0]->nn_shortText;
                    }
                }
            }
            unset($xml);
        }

        try { $xmlNapoved = simplexml_load_file('http://meteo.arso.gov.si/uploads/probase/www/fproduct/text/sl/fcast_si_text.xml'); } catch (Exception $e) {}
        if (!empty($xmlNapoved)) {
            $data["forecast"] += [
                "forecasts" => array_map('strval', array_merge($xmlNapoved->xpath("//section[@id='fcast_SLOVENIA_d1']/para"),
                                                               $xmlNapoved->xpath("//section[@id='fcast_SLOVENIA_d2']/para"))),

                "prospects"   => array_map('strval', $xmlNapoved->xpath("//section[@id='fcast_SLOVENIA_d3-d5']/para")),
                "warning" => array_map('strval', $xmlNapoved->xpath("//section[@id='warning_SLOVENIA']/para")),
            ];
        }

        return $data;
    });
    
    return view('info', ['data' => $dataCache]);
});
